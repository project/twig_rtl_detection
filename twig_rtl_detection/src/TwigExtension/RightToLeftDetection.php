<?php

namespace Drupal\twig_rtl_detection\TwigExtension;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class definition for RightToLeftDetection service.
 */
class RightToLeftDetection extends AbstractExtension {

  /**
   * TRUE if the current language is RTL, FALSE otherwise.
   *
   * @var bool
   */
  private $rtl;

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('is_language_rtl', [$this, 'isRightToLeft']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(LanguageManagerInterface $languageManager) {
    $this->rtl = $languageManager->getCurrentLanguage()->getDirection() == LanguageInterface::DIRECTION_RTL;
  }

  /**
   * Indicates whether the current language is RTL or not.
   *
   * @return bool
   *   TRUE if the current language is RTL, FALSE otherwise.
   */
  public function isRightToLeft() {
    return $this->rtl;
  }

}
